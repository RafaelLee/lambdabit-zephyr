#ifndef __LAMBDABIT_OBJECT_H__
#define __LAMBDABIT_OBJECT_H__
/*  Copyright (C) 2020
 *        "Mu Lei" known as "NalaGinrut" <NalaGinrut@gmail.com>
 *  Lambdabit is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or  (at your option) any later version.

 *  Lambdabit is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.

 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this program.
 *  If not, see <http://www.gnu.org/licenses/>.
 */

#include "types.h"
#include "qlist.h"
#include "gc.h"

/*
  Bits:         0                     15                     31
  0.  Imm Int       |                     32bit int               |
  1.  Arbi Int      |      next cell       |      16bit int       |
  2.  Closure       |      entry offset    |      env offsert     |
  3.  Pair          |      car             |      cdr             |
  4.  Symbol        |      interned head pointer                  |
  5.  String        |      head pointer                           |
  6.  Vector        |      length          |      content         |
  7.  Continuation  |      parent          |      closure         |
  8.  List          |      length          |      content         |
  9.  Boolean       |      true/false const                       |

  63. Special       |      const encoding                         |
*/

typedef enum obj_type
  {
   imm_int = 0, arbi_int,
   closure, pair, symbol, string, vector, continuation,
   list, boolean,
   special = 63
  } otype_t;

extern const u8_t true_const;
extern const u8_t false_const;
extern const u8_t null_const;
extern const u8_t none_const;

typedef union ObjectAttribute
{
  struct
  {
    /* NOTE:
       Some object contains 2 fields, so GC has least 2 bits.
    */
    unsigned gc: 2;
    unsigned type: 6;
  };
  u8_t all;
} __packed oattr;

typedef struct Object
{
  oattr attr;
  void* value;
} __packed *object_t, Object;

typedef union Continuation
{
  struct
  {
    unsigned closure: 16; // the offset in ss to store closure
    unsigned parent: 16; // the offset in ss to store parent
  };
  u32_t all;
}__packed *cont_t;


typedef union Closure
{
  struct
  {
    unsigned entry: 16; // the offset in ss to entry
    unsigned env: 16; // the offset in ss to env
  };
  u32_t all;
}__packed *closure_t;

typedef struct Vector
{} __packed *vec_t;

/* define Page List */
typedef SLIST_HEAD(ObjectListHead, ObjectList) obj_list_head_t;

typedef struct ObjectList
{
  SLIST_ENTRY(ObjectList) obj_list;
  object_t obj;
} __packed obj_list_t;

static inline bool object_is_false(object_t obj)
{
  return (boolean == obj->attr.type) && (obj->value == (void*)&true_const);
}

static inline bool object_is_true(object_t obj)
{
  return !object_is_false(obj);
}

static inline u8_t vector_ref(vec_t vec, u8_t index)
{
  /* TODO
     NOTE:
     * The return value must be u8_t which is the address in ss, otherwise we can't
     push it into the dynamic stack.
     * So the fetched value must be stored into ss.
     */

  return 0;
}

static inline void vector_set(vec_t vec, u8_t index, object_t value)
{
  // TODO
}

void init_predefined_objects(void);
void free_object(object_t obj);

#endif // End of __LAMBDABIT_OBJECT_H__
