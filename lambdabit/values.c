/*  Copyright (C) 2020
 *        "Mu Lei" known as "NalaGinrut" <NalaGinrut@gmail.com>
 *  Lambdabit is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or  (at your option) any later version.

 *  Lambdabit is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.

 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this program.
 *  If not, see <http://www.gnu.org/licenses/>.
 */

#include "values.h"

// -------- Continuation
object_t make_continuation()
{
  object_t obj = (object_t)gc_malloc(sizeof(struct Object));
  cont_t cont = (cont_t)gc_malloc(sizeof(union Continuation));

  obj->attr.gc = 0;
  obj->attr.type = (otype_t)continuation;
  obj->value = (void*)cont;

  return obj;
}

// ----------- Closure
object_t make_closure(u16_t env, u16_t entry)
{
  object_t obj = (object_t)gc_malloc(sizeof(struct Object));
  closure_t closure = (closure_t)gc_malloc(sizeof(union Closure));

  closure->env = env;
  closure->entry = entry;
  obj->attr.gc = 0;
  obj->attr.type = (otype_t)closure;
  obj->value = (void*)closure;

  return obj;
}
